package models

import (
	"html"
	"strings"
	"time"

	"golang.org/x/crypto/bcrypt"
)

type User struct {
	Id       int        `json:"id" gorm:"column:id;"`
	Name     string     `json:"name" gorm:"column:name;"`
	UserName string     `json:"username" gorm:"column:username;"`
	Password string     `json:"password" gorm:"column:password;"`
	CreateAt *time.Time `json:"create_at" gorm:"column:create_at;"`
	UpdateAt *time.Time `json:"update_at" gorm:"column:update_at;"`
}

func Hash(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func CheckPasswordHash(hashedPassword, password string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
}

func Santize(data string) string{
	data = html.EscapeString(strings.TrimSpace(data))
	return data
}