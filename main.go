package main

import (
	// "fmt"
	// "net/http"
	db "day1/database"
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type Users struct {
	Id       int       `json:"id" gorm:"column:id;"`
	Name     string    `json:"name" gorm:"column:name;"`
	Email    string    `json:"username" gorm:"column:email;"`
	Password string    `json:"password" gorm:"column:password;"`
	CreateAt time.Time `json:"create_at" gorm:"column:create_at;"`
	UpdateAt time.Time `json:"update_at" gorm:"column:update_at;"`
}

func main() {
	db := db.Connected()
	r := gin.Default()
	api := r.Group("/api")
	{
		api.GET("/users", ListUsers(db.DB))
		api.POST("/login", CheckLogin(db.DB))
		api.POST("/register", Register(db.DB))
	}
	r.Run()
}
func CheckLogin(db *gorm.DB) func(*gin.Context) {
	return func(ctx *gin.Context) {

		email := ctx.PostForm("email")
		password := ctx.PostForm("password")
		var result Users
		if err := db.Where("email=?", email).Where("password=?", password).First(&result).Error; err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": "Email or Password incorrect",
			})
			return
		}

		ctx.JSON(http.StatusOK, gin.H{
			"data": result,
		})
	}
}

func Register(db *gorm.DB) func(*gin.Context) {
	return func(ctx *gin.Context) {

		email := ctx.PostForm("email")
		name := ctx.PostForm("name")
		password := ctx.PostForm("password")
		result := Users{Name: name, Email: email, Password: password, CreateAt: time.Now(), UpdateAt: time.Now()}
		if err := db.Create(&result).Error; err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": "Register has failed",
			})
			return
		}

		ctx.JSON(http.StatusOK, gin.H{
			"data": result,
		})
	}
}

func ListUsers(db *gorm.DB) func(*gin.Context) {
	return func(ctx *gin.Context) {
		var result []Users
		if err := db.Find(&result).Error; err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
		}
		ctx.JSON(http.StatusOK, gin.H{
			"data": result,
		})
	}
}
