package db

import (
	"log"
	"os"

	"github.com/joho/godotenv"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

type Database struct {
	DB *gorm.DB
}

func Connected() *Database {
	envErr := godotenv.Load(".env")
	if envErr != nil {
		log.Fatalln("Cannot connect to MySQL")
		os.Exit(0)
	}
	dsn := os.Getenv("MYSQL_CONNECTION")

	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})

	if err != nil {
		log.Fatalln("Cannot connect to MySQL:", err)
	}

	log.Println("Connected to MySQL:", db)
	return &Database{DB: db}
}
